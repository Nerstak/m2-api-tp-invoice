# Invoice PDF generation

## Questions

### Question 1
- Fastify ignores (and strip away) unknown properties in request
- Fastify throws a 400 (Bad Request) error if a property is missing in request
- If a property is null, it will replace it with the default value of the type

### Question 2
- Fastify ignores (and strip away) unknown properties in response
- Fastify throws a 500 (Internal Server) error if a property is missing in response

### Question 3
Input validation is not only depending on schema validation. It has to verify validity of specific value (eg: an email field should have an email value), the coherence of those values (eg: a quantity should not be negative), type conversion (a field marked as a date should be converted to a Date).
JSON Schema validation can do some validation (regex, min-max, range, etc), but not all of them (such as type validation)
Also, schemas and types cannot check the quality of input values (prevent XSS injection, sanitizing), or perform file validation (forbidden content, malware check, etc.)

See: https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html