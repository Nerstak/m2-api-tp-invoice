import { FastifyInstance } from 'fastify'
import { Invoice } from '../schemas/types/invoice'
import * as invoiceSchema from '../schemas/json/invoice.json'
import ReactDOMServer from 'react-dom/server';
import show from "../templates/invoices/show";

enum MIME_TYPES {
  HTML = 'text/html',
  JSON = 'application/json',
  PDF = 'application/pdf'
}

export async function invoicesRoutes (fastify: FastifyInstance) {
  fastify.route<{ Body: Invoice }>({
    method: 'POST',
    url: '/',
    schema: {
      body: invoiceSchema,
      response: { 200: invoiceSchema }
    },
    handler: async function (request, reply): Promise<Invoice> {
      switch (request.headers.accept) {
        case MIME_TYPES.JSON:
          return request.body;

        case MIME_TYPES.PDF:
          throw new Error('PDF are currently not implemented');

        default: {
          const totalTaxFree = request.body.itemInvoices.map(i => i.quantity * i.unitPriceWithoutTax).reduce((a, b) => a + b, 0);
          const totalTaxes = request.body.itemInvoices.map(i => i.quantity * (i.unitPriceWithoutTax / 100.0)* i.taxPercent).reduce((a, b) => a + b, 0);
          const renderedJSX = show(request.body, totalTaxFree, totalTaxes);
          return reply.type(MIME_TYPES.HTML).send(ReactDOMServer.renderToString(renderedJSX))
        }
      }
    }
  })
}