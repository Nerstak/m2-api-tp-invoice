import * as React from 'react';
import {Invoice} from '../../schemas/types/invoice';
import {Address} from "../../schemas/types/address";
import {ItemInvoice} from "../../schemas/types/item-invoice";
import { addDays, format } from 'date-fns';



export default function show(invoice: Invoice, totalTaxFree: number, totalTaxes: number) {
    return <div>
        <style>{`
            * {font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;}
            h1, h2 { font-weight: bold; }
            .root-box {margin: auto; padding: 30px;border: 1px solid #eee;}
            .address-box { font-style: italic; color: #363636 }
            .contacts {
                display: flex;
                justify-content: space-between;
            }
            .contact-box {
                line-height: 1px;
            }
            
            .total-box {
                text-align: right;
            }
            
            table, th, td {
                border: 1px solid black;
                width: 100%;
            }
            
            table {
                border-collapse: collapse;
            }
            
        `}</style>
        <div className="root-box">
            <h1 className="title">Runtime LTD</h1>
            {invoice.reference}
            {invoice.date}
            {showContacts(invoice)}
            {showTable(invoice.itemInvoices)}
            {showTotal(totalTaxFree, totalTaxes)}

            {showPaymentInfos(invoice)}
        </div>

    </div>
}

function showTable(items: ItemInvoice[]) {
    return <table>
        <thead>
            <tr>
                <th>Description</th>
                <th>Quantity</th>
                <th>Unit price (tax free)</th>
                <th>Total (tax free)</th>
            </tr>
        </thead>
        <tbody>
        {items.map(item => {
            return <tr>
                <td>{item.description}</td>
                <td>{item.quantity}</td>
                <td>{item.unitPriceWithoutTax}</td>
                <td>{item.unitPriceWithoutTax * item.quantity}</td>
            </tr>
        })}
        </tbody>
    </table>
}

function showContacts(invoice: Invoice) {
    return <div className="contacts">
        <div className="contact-Box">
            <p>{invoice.biller.email}</p>
            <p>{invoice.biller.phoneNumber}</p>
            {showAddress(invoice.biller.address)}
        </div>

        <div className="contact-Box">
            <p>{invoice.customer.name}</p>
            {showAddress(invoice.biller.address)}
        </div>
    </div>
}

function showAddress(address: Address) {
    return <div className="address-box">
            <p>{address.country}</p>
            <p>{address.state}</p>
            <p>{address.street}</p>
            <p>{address.city}, {address.zipcode}</p>
            <p>{address.additional}</p>
        </div>
}

function showTotal(totalTaxFree: number, totalTaxes: number) {
    return <div className="total-box">
        <p>Tax free total: {totalTaxFree}</p>
        <p>Taxes total: {totalTaxes}</p>
        <p>Total: {totalTaxFree + totalTaxes}</p>
    </div>
}

function showPaymentInfos(invoice: Invoice) {
    return <div>
        <p>To be paid before: {format(addDays(new Date(invoice.date), invoice.paymentDelayInDays), 'yyyy-MM-dd')}</p>
        <p>{invoice.biller.invoiceLegalFooter}</p>
    </div>
}
