import {expect} from 'chai';
import {fastify} from '../../lib/fastify';
import 'mocha';
import faker from 'faker';
import {Invoice} from "../../schemas/types/invoice";
import {Address} from "../../schemas/types/address";
import {format} from "date-fns";
import {ItemInvoice} from "../../schemas/types/item-invoice";


describe('Invoice API Routes', () => {
    it('should return HTML if valid by default', (done) => {
        fastify.inject({
            method: 'POST',
            url: '/invoices',
            payload: generateInvoiceJSON()
        }).then(resp => {
            expect(resp.statusCode).to.eq(200);
            // Yeah this doesnt work. I wanted to check if content is html, but idk how to do it
            //expect(resp.payload).to.match(/^(<([^>]+)>)$/i);
            done();
        }).catch(err => {
            done(err);
        });
    });

    it('should return JSON if asked', (done) => {
        const payload = generateInvoiceJSON();
        fastify.inject({
            method: 'POST',
            headers: {
                accept: 'application/json'
            },
            url: '/invoices',
            payload: payload
        }).then(resp => {
            expect(resp.statusCode).to.eq(200);
            expect(JSON.parse(resp.payload)).to.have.deep.equal(payload);
            done();
        }).catch(err => {
            done(err);
        });
    });

    it('shouldnt return PDF if asked', (done) => {
        const payload = generateInvoiceJSON();
        fastify.inject({
            method: 'POST',
            headers: {
                accept: 'application/pdf'
            },
            url: '/invoices',
            payload: payload
        }).then(resp => {
            expect(resp.statusCode).to.eq(500);
            done();
        }).catch(err => {
            done(err);
        });
    });
});

function generateInvoiceJSON():Invoice {
    return  {
        customer: {
            name: faker.name.findName(),
            address: generateAddressJSON()
        },
        date: format(faker.date.past(1, new Date()), 'yyyy-MM-dd'),
        itemInvoices: generateItemJSON(),
        paymentDelayInDays: faker.datatype.number({min:0, max:365, precision: 1}),
        reference: faker.datatype.uuid(),
        biller: {
            address: generateAddressJSON(),
            phoneNumber: faker.phone.phoneNumber(),
            email: faker.internet.email(),
            invoiceLegalFooter: faker.lorem.sentence()
        }
    }
}

function generateItemJSON(): ItemInvoice[] {
    const items: ItemInvoice[] = [];
    const qt: number = faker.datatype.number({min:1, max:10, precision: 1});

    for(let i = 0; i < qt ; i++ ) {
        items.push({
            description: faker.commerce.productName(),
            quantity: faker.datatype.number({min:1, max:10, precision: 1}),
            taxPercent: faker.datatype.number({min:0}),
            unitPriceWithoutTax: faker.datatype.number({min:1})

        })
    }
    return items;
}

function generateAddressJSON(): Address {
    return {
        street: faker.address.streetName(),
        city: faker.address.city(),
        zipcode: faker.address.zipCode(),
        country: faker.address.country(),
        state: faker.address.state(),
        additional: faker.address.direction()
    };
}